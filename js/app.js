async function fet() {
  var nombre = document.getElementById("form_name").value;
  const url = "http://localhost:3000/covidgt";
  var data = { nombre: nombre };
  fetch(url, {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => response.json())
    .then((data) => {
      coldata(data);
    })
    .catch((err) => console.log(err));
}

//--------------- Function for arraw data
async function coldata(arr) {
  const data = Object.entries(arr);
  if (data == "") {
    alert("Ingrese el nombre de un departamento");
  }else{
  data.forEach(([key, value]) => {
    graf(value.dep, value.positivos, value.recuperados, value.fallecidos);
  });
  }
}
//--------------- Function for drawing
async function graf(dep, pos, rec, fall) {
  const ctx = document.getElementById("myChart").getContext("2d");
  var opt = {
    title: {
      display: true,
      text: dep,
      fontSize: 18,
      fontStyle: "bold",
    },
    legend: {
      display: true,
      labels: {
        fontColor: "black",
        fontSize: 16,
      },
    },
    elements: {
      line: {
        tension: 0.5,
      },
    },
    layout: {
      padding: {
        left: 50,
        right: 0,
        top: 0,
        bottom: 5,
      },
    },
  };
  var oilData = {
    labels: ["Positivos", "Recuperados", "fallecidos"],
    datasets: [
      {
        data: [pos, rec, fall],
        backgroundColor: ["#ffce56", "#36a2eb", "#ff6384"],
      },
    ],
  };
  const myChart = new Chart(ctx, {
    type: "pie",
    data: oilData,
    options: opt,
  });
}
